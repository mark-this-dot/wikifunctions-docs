# What is a function?

This repo is for the single page website to introduce non-technical users to functions.

## Tech stack

This project uses [Vue 3 in Vite](https://vitejs.dev/guide/).

## How to run the project locally

In the command line:

1. Clone the repository
2. Then `cd` into the project directory.
3. Install dependencies with `npm install`
4. Run the project using `npm run dev`

The local server will start up at `http://localhost:3000/`

## Translations

This project uses the [vue-i18n plugin](https://www.npmjs.com/package/vue-i18n) for Internationalization.

In the `src/i18n` directory, you will find the Arabic and English JSON files for all of the content on the site.

In the `src/App.vue` file, you will find the `useI18n` hook which is used for translations.

```js
import { useI18n } from "vue-i18n";

const { t, locale } = useI18n();
```

There is also a `watch` to detect for `rtl` page direction.

```js
watch(locale, () => {
  const page = document.querySelector("html");
  page.lang = locale;
  page.dir = t("dir") || "rtl";
});
```

## Illustrations

All of the illustrations can be found in the `public/images` directory.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar)
